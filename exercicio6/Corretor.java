import java.util.ArrayList;
public class Corretor
{
    
    private String endereco;
    private double preco;
    private Imovel[] imovel;
    private ArrayList<Imovel> imovelList;
    
   
    public Corretor()
    {
      imovel = new Imovel[10];
      imovelList = new ArrayList<>();
    }
    
    public ArrayList<Imovel> getImovelList() {
        return imovelList;
    }
    
    public void addImovel(Imovel imovel) {
        this.imovelList.add(imovel);
    }
    public String getEndereco() {
        return this.endereco;
    }
    
    public void setEndereco(String paramEndereco) {
        this.endereco = paramEndereco;
    }
    
    public double setPreco() {
    return this.preco;    
    }
        
    public double getPreco() {
        return this.preco;
    }
    
    public void calculaComissaoCorretor() {
        
    }
}
